<?php
function whoop_geodir_event_template_loader($template) {

    if(geodir_get_current_posttype() == 'gd_event'){
        remove_filter('geodir_detail_page_sidebar_content', 'geodir_event_detail_page_sitebar_content', 2);
    }

    return $template;
}


function whoop_event_listing_template($template)
{
    global $preview;
    $listing_type = isset($_REQUEST['listing_type']) ? $_REQUEST['listing_type'] : '';
    if (($preview && $listing_type == 'gd_event') || (get_query_var('post_type') == 'gd_event')) {
        remove_action('geodir_detail_before_main_content', 'geodir_whoop_big_header', 30);
        $template = locate_template(array("geodirectory/listing-eventdetail.php"));
    }
    return $template;
}

add_filter('geodir_template_detail', 'whoop_event_listing_template');
add_filter('geodir_template_preview', 'whoop_event_listing_template');


function get_event_date_from_post($post)
{
    global $preview;

    if ($preview) {
        $event_start_date = $post->event_start ? date_i18n('l, F j, Y', strtotime($post->event_start)) : '';
        $event_start_time = $post->starttime ? date_i18n('g:i a', strtotime($post->starttime)) : '';

        $event_end_date = $post->event_end ? date_i18n('l, F j, Y', strtotime($post->event_end)) : '';
        $event_end_time = $post->endtime ? date_i18n('g:i a', strtotime($post->endtime)) : '';
    } else {
        $event_details = maybe_unserialize($post->recurring_dates);

        $event_start_date = $event_details['event_start'] ? date_i18n('l, F j, Y', strtotime($event_details['event_start'])) : '';
        $event_start_time = $event_details['starttime'] ? date_i18n('g:i a', strtotime($event_details['starttime'])) : '';

        $event_end_date = $event_details['event_end'] ? date_i18n('l, F j, Y', strtotime($event_details['event_end'])) : '';
        $event_end_time = $event_details['endtime'] ? date_i18n('g:i a', strtotime($event_details['endtime'])) : '';
    }

    return '<span class="eve-start-date">' . $event_start_date . ' ' . $event_start_time . ' - </span><span class="eve-end-date">' . $event_end_date . ' ' . $event_end_time . '</span>';
}

function whoop_event_singe_main_content($post)
{
    global $preview;
    ?>
    <div class="whoop-event-detail">
        <div class="whoop-event-photos">
            <div class="whoop-event-photo-inner">
                <?php
                if ($preview) {
                    $image_url_string = $post->post_images;
                    if (!empty($image_url_string)) {
                        $image_urls = explode(',', $image_url_string);
                        $image_url = $image_urls[0];
                        ?>
                        <img width="200" src="<?php echo $image_url; ?>" class="attachment-250x200 wp-post-image" alt="">
                        <?php
                    }
                } else {
//                    if (has_post_thumbnail()) {
//                        echo get_the_post_thumbnail( $post->ID, array( 250, 200) );
//                    }
                    if ($fimage = geodir_get_featured_image($post->ID, '', true, $post->featured_image)) {
                        ?>
                        <img width="200" src="<?php echo $fimage->src; ?>" class="attachment-250x200 wp-post-image" alt="">
                        <?php
                    }
                }
                ?>
            </div>
        </div>
        <h1 class="entry-title geodir-big-header-title fn whoop-title">
            <?php
            echo esc_attr(stripslashes($post->post_title));
            ?>
        </h1>
        <div class="whoop_event_details">
            <dl>
                <dt><?php echo __('Category:', GEODIRECTORY_FRAMEWORK); ?></dt>
                <dd>
                    <?php
                    $post_type = 'gd_event';
                    $post_taxonomy = $post_type . 'category';
                    if($preview) {
                        $cats = $post->post_category[$post_taxonomy];
                    } else {
                        $cats = $post->$post_taxonomy;
                    }

                    if (!empty($cats)) {
                        $links = array();
                        $terms = array();
                        $termsOrdered = array();
                        if (!is_array($cats)) {
                            $post_term = explode(",", trim($cats, ","));
                        } else {
                            $post_term = $cats;
                        }

                        $post_term = array_unique($post_term);
                        if (!empty($post_term)) {
                            foreach ($post_term as $pt) {
                                $pt = trim($pt);

                                if ($pt != ''):
                                    $term = get_term_by('id', $pt, $post_taxonomy);
                                    if (is_object($term)) {
                                        $links[] = "<a href='" . esc_attr(get_term_link($term, $post_taxonomy)) . "'>$term->name</a>";
                                        $terms[] = $term;
                                    }
                                endif;
                            }
                            // order alphabetically
                            asort($links);
                            foreach (array_keys($links) as $key) {
                                $termsOrdered[$key] = $terms[$key];
                            }
                            $terms = $termsOrdered;

                        }

                        if (!isset($listing_label)) {
                            $listing_label = '';
                        }
                        $taxonomies[$post_taxonomy] = wp_sprintf('%l', $links, (object)$terms);

                    }


                    if (isset($taxonomies[$post_taxonomy])) {
                        echo '<span class="geodir-category">' . $taxonomies[$post_taxonomy] . '</span>';
                    }
                    ?>
                </dd>
            </dl>
            <dl>
                <dt>
                    <?php echo __('When:', GEODIRECTORY_FRAMEWORK); ?>
                </dt>
                <dd class="eve-dates">
                    <?php
                    echo get_event_date_from_post($post);
                    ?>
                </dd>
            </dl>
            <dl>
                <dt>
                    <?php echo __('Where:', GEODIRECTORY_FRAMEWORK); ?>
                </dt>
                <dd>
                    <?php
                    echo  whoop_get_address_html($post);
                    ?>
                </dd>
            </dl>
            <dl>
                <dt>
                    <?php echo __('Submitted By:', GEODIRECTORY_FRAMEWORK); ?>
                </dt>
                <dd class="eve-submitted-by">
                    <?php
                    if($preview) {
                        $author_id = get_current_user_id();
                    } else {
                        $author_id = $post->post_author;
                    }
                    $user = get_user_by('id', $author_id);
                    ?>
                    <div class="eve-submitted-by-avatar">
                        <a href="<?php echo whoop_get_user_profile_link($user->ID); ?>"><?php echo get_avatar($user->ID, 20); ?></a>
                    </div>
                    <a class="eve-user-name" href="<?php echo whoop_get_user_profile_link($user->ID); ?>">
                        <?php echo whoop_bp_member_name(whoop_get_current_user_name($user)); ?>
                    </a>

                    <a href="<?php echo whoop_get_user_profile_link($user->ID); ?>events/" class="smaller">
                        <?php echo __('See all of', GEODIRECTORY_FRAMEWORK); ?> <?php echo whoop_bp_member_name(whoop_get_current_user_name($user)); ?>'s <?php echo __('events', GEODIRECTORY_FRAMEWORK); ?> &raquo;
                    </a>
                </dd>
            </dl>
            <?php
            $enable_what_why = apply_filters('whoop_events_enable_what_why', false);
            if ($enable_what_why) {
            ?>
            <dl>
                <dt class="eve-desc">
                    <?php echo __('What/Why:', GEODIRECTORY_FRAMEWORK); ?>
                </dt>
                <dd class="eve-desc">
                    <?php
                    if ($preview) {
                        $desc = $post->post_desc;
                    } else {
                        $desc = $post->post_content;
                    }
                    $show_editor = get_option('geodir_tiny_editor_on_add_listing');
                    $desc = $show_editor ? stripslashes($desc) : stripslashes($desc);
                    echo $desc;
                    ?>
                </dd>
            </dl>
            <?php } ?>
        </div>
    </div>
<?php
}

add_action('geodir_event_details_main_content', 'whoop_event_singe_main_content',50);
add_action('geodir_event_details_main_content', 'geodir_show_detail_page_tabs', 60);

function get_rsvp_user_avatar($user, $class='') {
    ?>
    <li class="<?php echo $class; ?>">
        <div class="item-avatar">
            <a href="<?php echo whoop_get_user_profile_link($user->ID); ?>"><?php echo get_avatar($user->ID, 40); ?></a>
        </div>

        <div class="item">
            <div class="item-title">
                <a href="<?php echo whoop_get_user_profile_link($user->ID); ?>">
                    <?php echo whoop_bp_member_name(whoop_get_current_user_name($user)); ?>
                </a>
            </div>
        </div>
    </li>
<?php
}

function get_rsvp_users_for_a_post($post_id, $type = "event_rsvp_yes", $limit = 10)
{
    $ids = get_post_meta($post_id, $type, true);
    if (!is_array($ids)) {
        return;
    }
    $count = 1;
    foreach ($ids as $id) {
        if($count > $limit) {
            break;
        }
        $user = get_user_by('id', $id);
        get_rsvp_user_avatar($user);
        $count++;
    } ?>
<?php
}

function event_interested_people_count($post_id) {
    $yes_users = get_post_meta($post_id, 'event_rsvp_yes', true);
    $maybe_users = get_post_meta($post_id, 'event_rsvp_maybe', true);

    $yes_count = 0;
    $maybe_count = 0;
    if ($yes_users) {
        $yes_count = count($yes_users);
    }
    if ($maybe_users) {
        $maybe_count = count($maybe_users);
    }
    $total = $yes_count + $maybe_count;

    $count = array();
    $count['yes'] = $yes_count;
    $count['maybe'] = $maybe_count;
    $count['total'] = $total;

    return $count;
}

function event_is_current_user_interested($post_id) {
    if ( ! get_current_user_id() ) {
        return;
    }
    $current_user = wp_get_current_user();

    $yes_users = get_post_meta($post_id, 'event_rsvp_yes', true);
    $maybe_users = get_post_meta($post_id, 'event_rsvp_maybe', true);

    $type = null;
    if ($yes_users) {
        foreach ($yes_users as $uid) {
            if ($uid == $current_user->ID) {
                $type = 'event_rsvp_yes';
                break;
            }
        }
    }
    if ($maybe_users) {
        foreach ($maybe_users as $uid) {
            if ($uid == $current_user->ID) {
                $type = 'event_rsvp_maybe';
                break;
            }
        }
    }

    return $type;
}

function geodir_rsvp_add_or_remove($rsvp_args = array()) {
    if ( ! get_current_user_id() ) {
        return;
    }
    $current_user = wp_get_current_user();

    if ($rsvp_args['action'] == 'add') {
        $users = get_post_meta($rsvp_args['post_id'], $rsvp_args['type'], true);

        if(is_array($users))
            $users[$current_user->ID] = $current_user->ID;
        else
            $users = array($current_user->ID => $current_user->ID);

        update_post_meta($rsvp_args['post_id'], $rsvp_args['type'], $users);

        //for user events listing
        $posts = get_user_meta($current_user->ID, $rsvp_args['type'], true);

        if(is_array($posts))
            $posts[$rsvp_args['post_id']] = $rsvp_args['post_id'];
        else
            $posts = array($rsvp_args['post_id'] => $rsvp_args['post_id']);

        update_user_meta($current_user->ID, $rsvp_args['type'], $posts);

    } else {
        $users = get_post_meta($rsvp_args['post_id'], $rsvp_args['type'], true);

        if(is_array($users))
            unset($users[$current_user->ID]);

        update_post_meta($rsvp_args['post_id'], $rsvp_args['type'], $users);

        //for user events listing
        $posts = get_user_meta($current_user->ID, $rsvp_args['type'], true);

        if(is_array($posts))
            unset($posts[$rsvp_args['post_id']]);

        update_user_meta($current_user->ID, $rsvp_args['type'], $posts);
    }
    do_action('ayi_interested_update', $rsvp_args['post_id'], $current_user->ID, $rsvp_args['type'], $rsvp_args['action']);

}


function geodir_rsvp_add_on_event_creation($post_id, $post, $update) {

    if (!$update && $post->post_type == 'gd_event') {
        $rsvp_args = array();
        $rsvp_args['action'] = 'add';
        $rsvp_args['type'] = 'event_rsvp_yes';
        $rsvp_args['post_id'] = $post_id;

        geodir_rsvp_add_or_remove($rsvp_args);
    }
}
add_action('wp_insert_post', 'geodir_rsvp_add_on_event_creation', 10, 3);

function geodir_rsvp_remove_on_event_deletion($post_id) {
    $post = get_post($post_id);
    if ($post->post_type == 'gd_event') {
        $rsvp_args = array();
        $rsvp_args['action'] = 'remove';
        $rsvp_args['type'] = 'event_rsvp_yes';
        $rsvp_args['post_id'] = $post_id;

        geodir_rsvp_add_or_remove($rsvp_args);
    }
}
add_action( 'before_delete_post', 'geodir_rsvp_remove_on_event_deletion' );
add_action( 'wp_trash_post',      'geodir_rsvp_remove_on_event_deletion' );


function geodir_update_event_interested_count($post_id = 0)
{
    global $wpdb, $plugin_prefix;

    $post_type = 'gd_event';
    $detail_table = $plugin_prefix . $post_type . '_detail';

    $count = event_interested_people_count($post_id);

    if ($wpdb->get_var("SHOW TABLES LIKE '" . $detail_table . "'") == $detail_table) {

        $wpdb->query(
            $wpdb->prepare(
                "UPDATE " . $detail_table . " SET
						rsvp_count = %d
						where post_id = %d",
                array($count['total'], $post_id)
            )
        );
    }
}
add_action('ayi_interested_update', 'geodir_update_event_interested_count', 20);

function event_list_content_from_post($post) {
    $author_id = $post->post_author;
    $user = get_user_by('id', $author_id);
    ?>
    <li>
        <div class="event-content-box">
            <div class="event-content-avatar">
                <div class="event-content-avatar-inner">
                    <?php
                    if ($fimage = geodir_get_featured_image($post->ID, '', true, $post->featured_image)) {
                        ?>
                        <a href="<?php echo get_the_permalink($post->ID); ?>">
                            <div class="geodir_thumbnail" style="background-image:url(<?php echo $fimage->src; ?>);"></div>
                        </a>
                    <?php } ?>
                </div>
            </div>
            <div class="event-content-body">
                <div class="event-content-body-top">
                    <div class="event-title">
                        <a href="<?php echo get_the_permalink($post->ID) ?>"><?php echo get_the_title($post->ID) ?></a>

                        <div class="event-date">
                            <?php echo get_event_date_from_post($post); ?>
                        </div>
                    </div>
                    <div class="event-author">
                        <div class="event-submitted-by">
                            <?php echo __('Submitted by', GEODIRECTORY_FRAMEWORK); ?><br/>
                            <a href="<?php echo whoop_get_user_profile_link($user->ID); ?>">
                                <?php echo whoop_bp_member_name(whoop_get_current_user_name($user)); ?>
                            </a>
                        </div>
                        <div class="event-submitted-by-avatar">
                            <a href="<?php echo whoop_get_user_profile_link($user->ID); ?>"><?php echo get_avatar($user->ID, 30); ?></a>
                        </div>

                    </div>

                </div>
                <div class="event-content-body-bottom">
                    <div class="event-address">
                        <?php
                        echo whoop_get_address_html($post);
                        ?>
                    </div>
                    <div class="event-interested">
                        <?php echo $post->rsvp_count; ?> <?php echo whoop_pluralize($post->rsvp_count, __('is interested', GEODIRECTORY_FRAMEWORK), __('are interested', GEODIRECTORY_FRAMEWORK)); ?>
                    </div>
                </div>
            </div>
        </div>
    </li>
<?php
}

function whoop_option_geodir_disable_rating()
{
    global $post, $gdf;
    if ($gdf['whoop-event-review'] == '1') {
        if(isset($post->post_type) && $post->post_type=='gd_event'){
            remove_action('comment_form_logged_in_after', 'geodir_comment_rating_fields');
            remove_action('comment_form_before_fields', 'geodir_comment_rating_fields');
            if (defined('GEODIRREVIEWRATING_VERSION')) {
                remove_action("comments_template",'geodir_reviewrating_show_post_ratings',10);
                remove_action( 'comment_form_logged_in_after', 'geodir_reviewrating_comment_rating_fields' );
                remove_action( 'comment_form_before_fields', 'geodir_reviewrating_comment_rating_fields' );
            }
            return 1;
        }
    }
}
add_filter('pre_option_geodir_disable_rating', 'whoop_option_geodir_disable_rating');

//function whoop_option_geodir_disable_rating_action() {
//    global $post, $gdf;
//    if ($gdf['whoop-event-review'] == '1') {
//        if(isset($post->post_type) && $post->post_type=='gd_event'){
//            add_filter('pre_option_geodir_disable_rating', '__return_true');
//        }
//    }
//}
//add_action('wp', 'whoop_option_geodir_disable_rating_action', 100);

function geodir_event_index_right_section()
{
    if (get_option('geodir_show_listing_right_section')) { ?>
        <div class="geodir-content-right geodir-sidebar-wrap">
            <?php dynamic_sidebar('event-index'); ?>
        </div>
    <?php }
}

function geodir_event_index_content_section()
{
    ?>
    <?php dynamic_sidebar('event-index-content'); ?>
<?php
}

function whoop_geodir_listings_remove_content()
{
    if (get_query_var('post_type') == 'gd_event' && isset($_GET['e_index'])) {
        remove_action('geodir_listings_content', 'geodir_action_listings_content', 10);
        remove_action('geodir_listings_sidebar_right_inside', 'geodir_listing_right_section', 10);
        remove_action('geodir_listings_page_title', 'geodir_action_listings_title', 10);
        add_action('geodir_listings_sidebar_right_inside', 'geodir_event_index_right_section', 10);
        add_action('geodir_listings_content', 'geodir_event_index_content_section', 10);
    }
}
add_action('wp', 'whoop_geodir_listings_remove_content');

include_once(get_template_directory() .'/whoop-widgets/events-are-you-interested-widget.php');
include_once(get_template_directory() .'/whoop-widgets/popular-events-widget.php');
add_filter( 'template_include', 'whoop_geodir_event_template_loader',0);

function whoop_geodir_event_loop_filter_where($where, $filter) {

    $day = date_i18n('w');

    $week_start = date_i18n('Y-m-d', strtotime('-'.$day.' days'));
    $week_end = date_i18n('Y-m-d', strtotime('+'.(6-$day).' days'));

    $next_week_start = date_i18n('Y-m-d', strtotime('+'.(7-$day).' days'));
    $next_week_end = date_i18n('Y-m-d', strtotime('+'.(7+(6-$day)).' days'));

    $week_after_next_start = date_i18n('Y-m-d', strtotime('+'.(14-$day).' days'));
    $week_after_next_end = date_i18n('Y-m-d', strtotime('+'.(14+(6-$day)).' days'));

    if ( $filter == 'this_week' ) {
        $where .= "AND (" . EVENT_SCHEDULE . ".event_date <= '".$week_end."' AND " . EVENT_SCHEDULE . ".event_enddate >='" . $week_start."') ";
    }

    if ( $filter == 'next_week' ) {
        $where .= "AND (" . EVENT_SCHEDULE . ".event_date <= '".$next_week_end."' AND " . EVENT_SCHEDULE . ".event_enddate >='" . $next_week_start."') ";
    }

    if ( $filter == 'week_after_next' ) {
        $where .= "AND (" . EVENT_SCHEDULE . ".event_date <= '".$week_after_next_end."' AND " . EVENT_SCHEDULE . ".event_enddate >='" . $week_after_next_start."') ";
    }
    return $where;
}
add_filter('geodir_event_listing_filter_where', 'whoop_geodir_event_loop_filter_where', 10, 2);