<?php
//widgets
if( bp_is_active( 'friends' )) {
    include_once(get_template_directory() .'/whoop-widgets/buddypress/bp-friends-widget.php');
}
if ( bp_is_active( 'activity' ) ) {
    include_once(get_template_directory() .'/whoop-widgets/buddypress/bp-recent-activity.php');
    include_once(get_template_directory() .'/whoop-widgets/buddypress/bp-event-updates.php');
}

function whoop_configure_buddypress() {
    whoop_bp_custom_fields(true);
}
add_action( 'after_switch_theme', 'whoop_configure_buddypress' );

// Hook for add custom fields
add_action( 'admin_init', 'whoop_bp_custom_fields' );
function whoop_bp_custom_fields($switch=false) {
    //allow only for admin
    if ( !current_user_can( 'manage_options' ) ) {
        return;
    }

    $action = false;
    if ($switch == true) {
        $action = 'switch';
    } elseif (isset($_GET['xaction'])) {
        if ($_GET['xaction'] == 'xprofile-fields-delete') {
            delete_option( 'whoop_xfields_created');
            $action = 'delete';
        } elseif ($_GET['xaction'] == 'xprofile-fields-create') {
            delete_option( 'whoop_xfields_created');
            $action = 'create';
        }
    }

    if(!$action) {
        return;
    }

    $xfields_created = get_option( 'whoop_xfields_created');

    if (!$xfields_created) {

        $your_headline_id = xprofile_get_field_id_from_name('Your Headline');
        $i_love_id = xprofile_get_field_id_from_name('I Love');
        $find_me_in_id = xprofile_get_field_id_from_name('Find Me In');
        $my_hometown_id = xprofile_get_field_id_from_name('My Hometown');
        $my_blog_or_website_id = xprofile_get_field_id_from_name('My Blog Or Website');
        $why_you_should_id = xprofile_get_field_id_from_name('Why You Should Read My Reviews');
        $my_second_fav_id = xprofile_get_field_id_from_name('My Second Favorite Website');
        $great_book_id = xprofile_get_field_id_from_name('The Last Great Book I Read');
        $my_first_concert_id = xprofile_get_field_id_from_name('My First Concert');
        $my_fav_movie_id = xprofile_get_field_id_from_name('My Favorite Movie');
        $my_last_meal_id = xprofile_get_field_id_from_name('My Last Meal On Earth');
        $dont_tell_id = xprofile_get_field_id_from_name('Dont Tell Anyone Else But');
        $most_recent_id = xprofile_get_field_id_from_name('Most Recent Discovery');
        $current_crush_id = xprofile_get_field_id_from_name('Current Crush');

        if ($action == 'delete') {

            if ($your_headline_id) {
                xprofile_delete_field($your_headline_id);
            }

            if ($i_love_id) {
                xprofile_delete_field($i_love_id);
            }

            if ($find_me_in_id) {
                xprofile_delete_field($find_me_in_id);
            }

            if ($my_hometown_id) {
                xprofile_delete_field($my_hometown_id);
            }

            if ($my_blog_or_website_id) {
                xprofile_delete_field($my_blog_or_website_id);
            }

            if ($why_you_should_id) {
                xprofile_delete_field($why_you_should_id);
            }

            if ($my_second_fav_id) {
                xprofile_delete_field($my_second_fav_id);
            }

            if ($great_book_id) {
                xprofile_delete_field($great_book_id);
            }

            if ($my_first_concert_id) {
                xprofile_delete_field($my_first_concert_id);
            }

            if ($my_fav_movie_id) {
                xprofile_delete_field($my_fav_movie_id);
            }

            if ($my_last_meal_id) {
                xprofile_delete_field($my_last_meal_id);
            }

            if ($dont_tell_id) {
                xprofile_delete_field($dont_tell_id);
            }

            if ($most_recent_id) {
                xprofile_delete_field($most_recent_id);
            }

            if ($current_crush_id) {
                xprofile_delete_field($current_crush_id);
            }

            update_option('whoop_xfields_created', 'yes');

            $query_args = array('page' => 'bp-profile-setup');
            $link = add_query_arg($query_args, admin_url('/users.php'));
            wp_redirect($link);

        }

        if ($action == 'create' OR $action == 'switch') {
            if (!$your_headline_id) {
                $name_field_args = array(
                    'field_id' => $your_headline_id,
                    'field_group_id' => 1,
                    'can_delete' => 1,
                    'type' => 'textbox',
                    'name' => 'Your Headline',
                    'description' => __('America\'s Next Top Singer, Don\'t you wish your girlfriend was Elite like me?', GEODIRECTORY_FRAMEWORK),
                );
                xprofile_insert_field($name_field_args);
            }

            if (!$i_love_id) {
                $name_field_args = array(
                    'field_id' => $i_love_id,
                    'field_group_id' => 1,
                    'can_delete' => 1,
                    'type' => 'textarea',
                    'name' => 'I Love',
                    'description' => __('Comma separated phrases (e.g. sushi, Radiohead, puppies)', GEODIRECTORY_FRAMEWORK),
                );
                xprofile_insert_field($name_field_args);
            }

            if (!$find_me_in_id) {
                $name_field_args = array(
                    'field_id' => $find_me_in_id,
                    'field_group_id' => 1,
                    'can_delete' => 1,
                    'type' => 'textbox',
                    'name' => 'Find Me In',
                    'description' => __('Greenwich Village, Nob Hill, or short pants', GEODIRECTORY_FRAMEWORK),
                );
                xprofile_insert_field($name_field_args);
            }

            if (!$my_hometown_id) {
                $name_field_args = array(
                    'field_id' => $my_hometown_id,
                    'field_group_id' => 1,
                    'can_delete' => 1,
                    'type' => 'textbox',
                    'name' => 'My Hometown',
                    'description' => __('Schenectady, NY', GEODIRECTORY_FRAMEWORK),
                );
                xprofile_insert_field($name_field_args);
            }

            if (!$my_blog_or_website_id) {
                $name_field_args = array(
                    'field_id' => $my_blog_or_website_id,
                    'field_group_id' => 1,
                    'can_delete' => 1,
                    'type' => 'textbox',
                    'name' => 'My Blog Or Website',
                    'description' => __('www.someblog.wordpress.com', GEODIRECTORY_FRAMEWORK),
                );
                xprofile_insert_field($name_field_args);
            }

            if (!$why_you_should_id) {
                $name_field_args = array(
                    'field_id' => $why_you_should_id,
                    'field_group_id' => 1,
                    'can_delete' => 1,
                    'type' => 'textbox',
                    'name' => 'Why You Should Read My Reviews',
                    'description' => __('I go out 7 times a week, sometimes 8; I\'m the king of the world!', GEODIRECTORY_FRAMEWORK),
                );
                xprofile_insert_field($name_field_args);
            }

            if (!$my_second_fav_id) {
                $name_field_args = array(
                    'field_id' => $my_second_fav_id,
                    'field_group_id' => 1,
                    'can_delete' => 1,
                    'type' => 'textbox',
                    'name' => 'My Second Favorite Website',
                    'description' => __('www.flickr.com, www.pets.com', GEODIRECTORY_FRAMEWORK),
                );
                xprofile_insert_field($name_field_args);
            }

            if (!$great_book_id) {
                $name_field_args = array(
                    'field_id' => $great_book_id,
                    'field_group_id' => 1,
                    'can_delete' => 1,
                    'type' => 'textbox',
                    'name' => 'The Last Great Book I Read',
                    'description' => __('Stephen Colbert\'s I Am America, Whatever Oprah tells me', GEODIRECTORY_FRAMEWORK),
                );
                xprofile_insert_field($name_field_args);
            }

            if (!$my_first_concert_id) {
                $name_field_args = array(
                    'field_id' => $my_first_concert_id,
                    'field_group_id' => 1,
                    'can_delete' => 1,
                    'type' => 'textbox',
                    'name' => 'My First Concert',
                    'description' => __('Duran Duran, Vanilla Ice, Wang Chung', GEODIRECTORY_FRAMEWORK),
                );
                xprofile_insert_field($name_field_args);
            }

            if (!$my_fav_movie_id) {
                $name_field_args = array(
                    'field_id' => $my_fav_movie_id,
                    'field_group_id' => 1,
                    'can_delete' => 1,
                    'type' => 'textbox',
                    'name' => 'My Favorite Movie',
                    'description' => __('Eat Drink Man Woman, Harold & Kumar Go to White Castle', GEODIRECTORY_FRAMEWORK),
                );
                xprofile_insert_field($name_field_args);
            }

            if (!$my_last_meal_id) {
                $name_field_args = array(
                    'field_id' => $my_last_meal_id,
                    'field_group_id' => 1,
                    'can_delete' => 1,
                    'type' => 'textbox',
                    'name' => 'My Last Meal On Earth',
                    'description' => __('French Laundry, My mom\'s meatloaf, A tub of fried chicken', GEODIRECTORY_FRAMEWORK),
                );
                xprofile_insert_field($name_field_args);
            }

            if (!$dont_tell_id) {
                $name_field_args = array(
                    'field_id' => $dont_tell_id,
                    'field_group_id' => 1,
                    'can_delete' => 1,
                    'type' => 'textbox',
                    'name' => 'Dont Tell Anyone Else But',
                    'description' => __('I love Starbucks; My mom nominated me for the Elite Squad', GEODIRECTORY_FRAMEWORK),
                );
                xprofile_insert_field($name_field_args);
            }

            if (!$most_recent_id) {
                $name_field_args = array(
                    'field_id' => $most_recent_id,
                    'field_group_id' => 1,
                    'can_delete' => 1,
                    'type' => 'textbox',
                    'name' => 'Most Recent Discovery',
                    'description' => __('Shoegazer Music, In-N-Out Animal Style', GEODIRECTORY_FRAMEWORK),
                );
                xprofile_insert_field($name_field_args);
            }

            if (!$current_crush_id) {
                $name_field_args = array(
                    'field_id' => $current_crush_id,
                    'field_group_id' => 1,
                    'can_delete' => 1,
                    'type' => 'textbox',
                    'name' => 'Current Crush',
                    'description' => __('Hayden Panettiere, Jessie\'s Girl', GEODIRECTORY_FRAMEWORK),
                );
                xprofile_insert_field($name_field_args);
            }
        }

        update_option('whoop_xfields_created', 'yes');

        if ($action == 'create') {
            $query_args = array('page' => 'bp-profile-setup');
            $link = add_query_arg($query_args, admin_url('/users.php'));
            wp_redirect($link);
        }
    }
}


