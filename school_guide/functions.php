<?php
/*#############################################
HERE YOU CAN ADD YOUR OWN FUNCTIONS OR REPLACE FUNCTIONS IN THE PARENT THEME
#############################################*/

// Here we add the parent styles
add_action( 'wp_enqueue_scripts', 'whoop_child_enqueue_styles' );
function whoop_child_enqueue_styles() {
    wp_enqueue_style( 'whoop-parent-style', get_template_directory_uri() . '/style.css', array(), GDF_VERSION);

}

// Here we defind the textdomain for the child theme, if changing you should also replace it in the function below. 
if (!defined('WHOOP_CHILD')) define('WHOOP_CHILD', 'whoop_child');


add_action('after_setup_theme', 'whoop_child_theme_setup');
function whoop_child_theme_setup(){
   // load_child_theme_textdomain( WHOOP_CHILD, get_stylesheet_directory() . '/languages' ); // uncomment this if you plan to use translation
}
function custom_register_admin_scripts() {

wp_register_script( 'custom-javascript', get_stylesheet_directory_uri() . '/custom.js' );
wp_enqueue_script( 'custom-javascript' );

} // end custom_register_admin_scripts
add_action( 'admin_enqueue_scripts', 'custom_register_admin_scripts' );

add_action('admin_head', 'my_custom_fonts');

function my_custom_fonts() {
  echo '<style>
    #geodir_placefilter_row{
  display: none;
}
  </style>';
}
?>
